class Tweet
	attr_reader :username, :body, :image_url
	attr_writer :image_url

	def initialize(username:, body:, image_url: nil)
		@username = username
		@body = body
		@image_url = image_url
	end
	
	def to_s
		"Tweet user=#{@username}, body=#{@body}"
  end
end