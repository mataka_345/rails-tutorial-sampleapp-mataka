# 実行例：
# ruby app/helpers/check_password_digest.rb '$2a$08$Izkr9AiGjtqvtRBM0cYQZ.ZtpKdamH9J6iUyYCpJYFRRIbeivkKlC' 'hogehoge'
# -> valid
# ruby app/helpers/check_password_digest.rb '$2a$08$Izkr9AiGjtqvtRBM0cYQZ.ZtpKdamH9J6iUyYCpJYFRRIbeivkKlC' 'fugafuga'
# -> invalid
# 注意）第一引数password_digestはシングルクォーテーションで囲む

# 1.コマンドライン引数を取得する
# password_digest = ARGV[0]
# unencrypted_password = ARGV[1]

# 2.bcrypt読み込む
# require 'bcrypt'

# 3.第一引数は第二引数をハッシュ化したものか判定し
# trueであれば'valid'、falseであれば'invalid'を出力する
# puts BCrypt::Password.new(password_digest).is_password?(unencrypted_password) ? 'valid' : 'invalid'
